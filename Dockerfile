FROM python:3.7-buster
WORKDIR /app
COPY . /app
RUN pip install --no-cache-dir -r requirements.txt
EXPOSE 4000
CMD ["python3", "app.py"]
