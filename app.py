import operator
from flask import Flask, render_template, request

app = Flask(__name__)


def operations(opr, arg1, arg2):
    arg1 = int(arg1)
    arg2 = int(arg2)
    valid_operations = ['add', 'truediv', 'mul', 'sub']
    if opr in valid_operations:
        func = getattr(operator, opr)
        res = func(arg1, arg2)
    else:
        res = None
    return res


@app.route('/')
def hello():
    arg1 = request.args.get('f')
    arg2 = request.args.get('s')
    opr = request.args.get('o')
    try:
        result = operations(opr, arg1, arg2)
    except Exception:
        result = None
    return render_template('main.html', result=result)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=4000)
