import psycopg2


class TestDB():

    def test_postgres(self):
        try:
            conn = psycopg2.connect("dbname=postgres user=postgres host=db")
        except Exception:
            conn = None
        assert(conn is not None)
