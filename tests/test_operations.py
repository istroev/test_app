import sys
import pytest
sys.path.append(sys.path[0] + "/..")
from app import operations


class TestOperations:

    def test_zero_error(self):
        with pytest.raises(ZeroDivisionError):
            operations('truediv', 1, 0)

    def test_add(self):
        assert(operations('add', 2, 2) == 4)

    def test_sub(self):
        assert(operations('sub', 2, 2) == 0)

    def test_mul(self):
        assert(operations('mul', 2, 3) == 6)

    def test_div(self):
        assert(operations('truediv', 10, 2) == 5)
