import requests


class TestRequests():

    def test_http_code(self):
        response = requests.get("http://web:4000")
        assert(response.status_code == 200)

    def test_add(self):
        payload = {"f": "2", "s": "2", "o": "add"}
        response = requests.get("http://web:4000", params=payload)
        assert("Your result: 4" in response.text)

    def test_sub(self):
        payload = {"f": "10", "s": "5", "o": "sub"}
        response = requests.get("http://web:4000", params=payload)
        assert("Your result: 5" in response.text)

    def test_mul(self):
        payload = {"f": "10", "s": "5", "o": "mul"}
        response = requests.get("http://web:4000", params=payload)
        assert("Your result: 50" in response.text)

    def test_div(self):
        payload = {"f": "10", "s": "5", "o": "truediv"}
        response = requests.get("http://web:4000", params=payload)
        assert("Your result: 2" in response.text)
